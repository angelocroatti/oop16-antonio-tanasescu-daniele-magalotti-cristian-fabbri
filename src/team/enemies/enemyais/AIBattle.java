package team.enemies.enemyais;

import battle_arena.BattleArena;

/**
 * 
 * @author Cristian
 *
 */

public interface AIBattle{
    
    public abstract void battleDecision(BattleArena battleArena);

}
